★ [[Overview.org][← Overview page]] ★

* Learning Orgdown

If you are familiar with other lightweight markup languages (LML), you
will find that Orgdown is only a little bit different [[https://karl-voit.at/2017/09/23/orgmode-as-markup-only/][but in a good
way]].

For novice users, the concept of lightweight markup languages has to
be explained briefly. [[https://en.wikipedia.org/wiki/Lightweight_markup_language][Wikipedia has a good article on the LWL topic]].
Please do read through it in order to understand the concept and the
purpose of LMLs. In short, it's a unified way to type certain things
in simple text. Orgdown is optimized for consistency and for easy
typing for humans even without [[Tool-Support.org][tool-support]].

This page is a tutorial on how to learn and remember Orgdown syntax
step-by-step.

* Simple text formatting

The most basic text formatting is to create *bold*, /italic/,
_underline_ and +strike through+ text. With =code= and ~commands~ you
can type in-line snippets of code, commands or things like variable names.

Five or more dashes produce a horizontal line:

When you type this ...

#+BEGIN_EXAMPLE
This is *in bold letters* or /italic ones/. You can _underline_ and even +strike through+ text.
If you need to write some in-line =code= or ~commands~, you can do this as well.

-----

Paragraphs are separated by one or multiple empty lines in-between.

In case you want to need to add small examples, you may prepend each line with a colon and a space:

: Here, Orgdown syntax is *not* highlighted any more.
: This way, you can add simple snippets as they are.
#+END_EXAMPLE

... you will get something like this:

This is *in bold letters* or /italic ones/. You can _underline_ and even +strike through+ text.
If you need to write some in-line =code= or ~commands~, you can do this as well.

-----

Paragraphs are separated by one or multiple empty lines in-between.

In case you want to need to add small examples, you may prepend each line with a colon and a space:

: Here, Orgdown syntax is *not* highlighted any more.
: This way, you can add simple snippets as they are.

** Syntax Highlighting

As you can see, the large boxes here show what you type into your text
editor. The following lines show how it is rendered at least here on
GitLab. You probably noticed that GitLab is not highlighting the
underlined word. Therefore, the highlighting on GitLab is not fully
compatible. Other tools that interpret Orgdown may apply similar
[[https://en.wikipedia.org/wiki/Syntax_highlighting][syntax highlighting]].

However, even though when you do see Orgdown syntax without syntax
highlighting at all, it does reflect some sort of obvious highlighting
of plain text anyhow.

Now that we've learned how to do some basic text formatting, we will
cover the other syntax elements of Orgdown in the following sections.

* Headings

When text grows in length, adding headings is a good way to structure your document.

In Orgdown, headings are pre-pended by a number of asterisk characters followed by at
least one space character. This space character makes sure that you do not mix up 
*bold words* that start a line with a heading. Clever and simple as well, isn't it?

One asterisk resembles a heading of level one, two asterisks for level two and so forth:

: * Heading of level 1
: ** Heading of level 2
: *** Heading of level 3
: *** Another heading of level 3
: ** Again a heading of level 2

Here, I just copied the first headings of this document so you can see
how it looks like when being typed:

: * Learning Orgdown
: 
: If you are familiar ...
: 
: * Simple text formatting
: 
: The most basic ...
: 
: ** Syntax Highlighting
: 
: As you can ...
: 
: * Headings
: 
: When text grows ...
:
: * Lists and Checkboxes

Please note that it is not mandatory to add empty lines between syntax
elements but it is considered as a nice touch because it makes it
easier for readers of the Orgdown text.

* Lists and Checkboxes

Lists are a nice way of structure stuff, brainstorm ideas or create a
simple shopping list with checkboxes.

In Orgdown, there are two different kinds of list items: unordered
list items with a dash and ordered list items that start with a number
and a dot. 

Both list items can have checkboxes that consists of a squared bracket
open, a space followed by a squared bracket close (like =[ ]=). If the
space character within a checkbox is replaced by a capital X, the
checkbox is marked as done (like =[X]=).

#+BEGIN_EXAMPLE
Orgdown is:
1. intuitive
   - easy to learn
   - simple to remember
2. standardized
3. consistent
4. easy to type
5. a good choice for a lightweight markup language

Therefore, I'm going to:

- [X] Learn about Orgdown
  1. Orgdown and Orgdown1
  2. Why this is a good idea
- [ ] read through the tutorial
  - [ ] make notes
  - [ ] try it myself
- [ ] start to adapt Orgdown1 in my daily life
#+END_EXAMPLE

Orgdown is:
1. intuitive
   - easy to learn
   - simple to remember
2. standardized
3. consistent
4. easy to type
5. a good choice for a lightweight markup language

Therefore, I'm going to:

- [X] Learn about Orgdown
  1. Orgdown and Orgdown1
  2. Why this is a good idea
- [ ] read through the tutorial
  - [ ] make notes
  - [ ] try it myself
- [ ] start to adapt Orgdown1 in my daily life

Pardon me the strong bias and the subliminal message in the example texts. ;-)

* Blocks

Remember the example from above?

: Here, Orgdown syntax is *not* highlighted any more.
: This way, you can add simple snippets as they are.

This would require you to type this:

#+BEGIN_EXAMPLE
: Here, Orgdown syntax is *not* highlighted any more.
: This way, you can add simple snippets as they are.
#+END_EXAMPLE

While this is a valid thing for one or few lines, the prefix string in
each line is utterly annoying when you do have more lines in your
snippet.

Orgdown has a good answer here to minimize your effort: blocks.

Blocks do not require a certain prefix for each line. They do require
a BEGIN string and an END string before and after the "block".

Since we would like to use blocks for different purposes such as
programming code snippets, quotes from famous poems or generic
examples, Orgdown has multiple block types that do vary in their begin
and end string.

** Example blocks

An EXAMPLE block is rendered "as is", keeping line breaks and not
interpreting content:

: #+BEGIN_EXAMPLE
: An example in an EXAMPLE block.
: Second line within this block.
: 
: This *is* an /example/ of _some_ syntax +highlighting+.
: #+END_EXAMPLE

#+BEGIN_EXAMPLE
An example in an EXAMPLE block.
Second line within this block.

This *is* an /example/ of _some_ syntax +highlighting+.
#+END_EXAMPLE

** Quote blocks

Content within a QUOTE block may get different line breaks when
exported/rendered and interprets Orgdown text formatting:

: #+BEGIN_QUOTE
: An example in an QUOTE block.
: Second line within this block.
: 
: This *is* an /example/ of _some_ syntax +highlighting+.
: #+END_QUOTE

#+BEGIN_QUOTE
An example in an QUOTE block.
Second line within this block.

This *is* an /example/ of _some_ syntax +highlighting+.
#+END_QUOTE

** Verse blocks

VERSE blocks a similar to quote blocks. However, in contrast to quote
blocks, they respect line breaks which makes them the appropriate
candidate for your favorite poems:

: #+BEGIN_VERSE
: An example in an VERSE block.
: Second line within this block.
: 
: This *is* an /example/ of _some_ syntax +highlighting+.
: #+END_VERSE

#+BEGIN_VERSE
An example in an VERSE block.
Second line within this block.

This *is* an /example/ of _some_ syntax +highlighting+.
#+END_VERSE

Please note that GitLab is ignoring VERSE blocks and therefore the
example verse block is not shown. It would look like the quote block
and additionally respecting line breaks of overlong lines.

** Source blocks

SRC blocks contain source code snippets. Text formatting is ignored,
line breaks preserved.

A Python source code example:

: #+BEGIN_SRC
:   def my_test(myvar: str = 'foo bar'):
:       """
:       This is an example function.
: 
:       @type  myvar: str = 'foo bar': number
:       @param myvar: str = 'foo bar': FIXXME
:       """
: 
:       mynewvar: str = myvar + ' additional content'
:       return mynewvar
: 
:   print("Hello " + my_text('Europe!'))
: #+END_SRC

#+BEGIN_SRC
  def my_test(myvar: str = 'foo bar'):
      """
      This is an example function.

      @type  myvar: str = 'foo bar': number
      @param myvar: str = 'foo bar': FIXXME
      """

      mynewvar: str = myvar + ' additional content'
      return mynewvar

  print("Hello " + my_text('Europe!'))
#+END_SRC

If you do feel the urge to add the information on the programming
language you might check out [[https://orgmode.org/manual/Structure-of-Code-Blocks.html][Org-mode which has this syntax element]].
In Orgdown, we want to keep it as simple as possible.

* Comments

One of the awesomeness of not using a [[https://en.wikipedia.org/wiki/WYSIWYG][WYSIWYG]] text processing solution
is the ability to insert syntax elements and comments in the same way
as you enter text.

Comments are very helpful for the author of a text. They can also be
used to temporarily hide unfinished text or keep notes that should not
be part of the final document.

** Comment lines

In Orgdown, comment lines contain content which is not visible in any
derived document such as a PDF document or a web view. Comment lines
start with optional spaces, followed by a hash character (=#=) and at
least one space character. Here are some examples:

#+BEGIN_EXAMPLE
hash space:

# This is a comment

space hash space:

 # This is a comment

space space hash space:

  # This is a comment
#+END_EXAMPLE

hash space:

# This is a comment

space hash space:

 # This is a comment

space space hash space:

  # This is a comment

** Comment blocks

In the block section above, I embezzled one additional type of block
for the sake of didactic: the comment block.

: #+BEGIN_COMMENT
: This is a multi line comment block.
: This is the second line.
: 
: This is the second paragraph.
: 
: This *is* an /example/ of _some_ syntax +highlighting+.
: #+END_COMMENT

#+BEGIN_COMMENT
This is a multi line comment block.
This is the second line.

This is the second paragraph.

This *is* an /example/ of _some_ syntax +highlighting+.
#+END_COMMENT

As with the blocks in general, this is helpful for a larger number of
lines you want to move into a comment.

* Links

In our hyperlinked world, links to web pages are an absolute must-have
for all kind of texts. In Orgdown, we do follow the following syntax:

#+BEGIN_EXAMPLE
- https://gitlab.com/publicvoit/orgdown → plain URL without brackets
- [[https://gitlab.com/publicvoit/orgdown]] → URL with brackets without description
- [[https://gitlab.com/publicvoit/orgdown][Orgdown homepage]] → URL with brackets with description

[[https://gitlab.com/publicvoit/orgdown][This *is* an /example/ of _some_ syntax +highlighting+ within =links= and ~such~.]]
#+END_EXAMPLE

- https://gitlab.com/publicvoit/orgdown → plain URL without brackets
- [[https://gitlab.com/publicvoit/orgdown]] → URL with brackets without description
- [[https://gitlab.com/publicvoit/orgdown][Orgdown homepage]] → URL with brackets with description

[[https://gitlab.com/publicvoit/orgdown][This *is* an /example/ of _some_ syntax +highlighting+ within =links= and ~such~.]]

A frequent issue when people start with Orgdown and links that do
contain a description text is that they can't remember what comes
first: the link or the link description text.

Here is a simple trick to remember the order: for any link to the web,
you need at least the URL itself. Any description without an URL does
not make any sense. The description text is optional as you can see
above. Therefore, the optional description can only follow the URL
when you take it logically.

So it's:

: [[URL][description]]

... and not the other way round.

Now it should be easy to remember for you, isn't it?

* Tables

In lightweight markup languages, tables are always a delicate topic.
Usually, it is tedious for a user to generate and maintain tables
without proper tool support. Therefore, if you do not use an editor
that supports Orgdown syntax for tables that, e.g., do align the
columns for you, you do need to pay attention to create syntactically
correct tables.

Tables do not have to be proper aligned as long as the correct number
of vertical bars per line is met:

: | *Heading1* | *head2* |
: |------------+---------|
: | entry      |      42 |
: | foo        |    21.7 |
: |------------+---------|
: | end        |   99.99 |

| *Heading1* | *head2* |
|------------+---------|
| entry      |      42 |
| foo        |    21.7 |
|------------+---------|
| end        |   99.99 |

Don't worry about alignment or the number of dashes if you don't have
proper tool support. This is a perfectly valid table:

: | *Heading1* | *head2* |
: |-+--|
: |entry|42|
: |foo|21.7|
: |-+-|
: |end|99.99|

| *Heading1* | *head2* |
|-+--|
|entry|42|
|foo|21.7|
|-+-|
|end|99.99|

Formatting is possible within table cells:

: | Example                                                 |
: |---------------------------------------------------------|
: | [[https://gitlab.com/publicvoit/orgdown][Orgdown]]                                                 |
: | This *is* an /example/ of _some_ syntax +highlighting+. |

| Example                                                 |
|---------------------------------------------------------|
| [[https://gitlab.com/publicvoit/orgdown][Orgdown]]                                                 |
| This *is* an /example/ of _some_ syntax +highlighting+. |

* Happy Orgdown-ing!

Congratulations, you have now mastered the basic syntax elements of
Orgdown. I hope you agree that for the most basic stuff, Orgdown is
really easy to type and remember.

If you need a brief example on the syntax elements, you can visit [[Orgdown1-Syntax-Examples.org][the
syntax example page]] which is also handy to check for Orgdown
compatibility in tools.

You might go back to the [[Overview.org][overview page]].
